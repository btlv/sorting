#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "helpers.h"
#include "mergesort.h"

#include <vector>
#include <iterator>

#define MINQSORTSIZE 8

template <typename Iter> struct Interval
{
    Iter begin;
    Iter end;
    Interval(const Iter& b, const Iter& e): begin(b), end(e) {}
    inline size_t length() const {return  end > begin ? end - begin : 0;}
    inline bool operator < (const Interval& o) const {return length() < o.length();}
    inline operator bool () const {return begin < end;}
};

template <typename Iter> inline void swapifless(const Iter& i)
{
    if(*(i + 1) < *i)
        mswap(*i, *(i + 1));
}

template<typename Iter> inline typename std::iterator_traits<Iter>::value_type getPivot(Iter begin, Iter end)
{
    typedef typename std::iterator_traits<Iter>::value_type T;
    const T pivotElems[2] = {*begin, *end};
    T pivotElem = pivotElems[0]/2 + pivotElems[1]/2;
    if(pivotElem < pivotElems[0] && pivotElem < pivotElems[1])
        pivotElem = pivotElems[1];
    return pivotElem;
}

template <typename Iter> void quicksort(const Iter& f, const Iter& l)
{
    size_t size = l > f ? l - f : 0;
    if(size < 2)
        return;
    if(size == 2)
    {
        swapifless(f);
        return;
    }
    Iter i(f);
    Iter j(l - 1);
    typedef typename std::iterator_traits<Iter>::value_type T;
    const T pivotElem = getPivot(i, j);
    while (i <= j)
    {
        while(*i < pivotElem && i <= j)
            ++i;
        while(pivotElem < *j && i <= j)
            --j;
        if(i <= j)
        {
            mswap(*i, *j);
            ++i;
            --j;
        }
    }

    Interval<Iter> intrvl[2] = {Interval<Iter>(f, j + 1), Interval<Iter>(i, l)};

    if(!intrvl[1] || intrvl[0] > intrvl[1])
        mswap<Interval<Iter> >(intrvl[0], intrvl[1]);

    quicksort(intrvl[0].begin, intrvl[0].end);

    //tail recursion on bigger part should prevent stack overflow on specially crafted worst cases
    //also it improves performance, we absolutely need it
    quicksort(intrvl[1].begin, intrvl[1].end);
}

template <typename Iter> void quicksortnr(const Iter& first, const Iter& last)
{
    const size_t MaxProbableStackDepth = 50;
    std::vector<Interval<Iter> > intervals;
    intervals.reserve(MaxProbableStackDepth);
    intervals.push_back(Interval<Iter>(first, last));
    typedef typename std::iterator_traits<Iter>::value_type T;
    while (!intervals.empty())
    {
        const Interval<Iter>& intrvl = intervals.back();
        const Iter f = intrvl.begin;
        const Iter l = intrvl.end;
        Iter i = f;
        Iter j = l - 1;
        const T pivotElem = getPivot(i, j);
        while (i <= j)
        {
            while(*i < pivotElem && i <= j)
                ++i;
            while(pivotElem < *j && i <= j)
                --j;
            if(i <= j)
            {
                mswap(*i, *j);
                ++i;
                --j;
            }
        }
        intervals.pop_back();
        if((l - f) > 2)
        {
            if(j > f)
                intervals.push_back(Interval<Iter>(f, j + 1));
            if(i < l - 1)
                intervals.push_back(Interval<Iter>(i, l));
        }
    }
}

#endif // QUICKSORT_H
