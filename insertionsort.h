#pragma once

#include "helpers.h"
#include <algorithm>
#include <cstring>

template <typename T> void insertionsort(T* t, size_t size)
{
    if(size < 2)
        return;

    //if(t[0] > t[1])
    //    swap(t[0], t[1]);

    size_t sorted = 1;
    while (sorted < size)
    {
        T& curElem = t[sorted];
        T* insertionPoint = std::upper_bound(t, &(t[sorted]), curElem);
        if(insertionPoint != &curElem)
        {
            size_t toMove = &curElem - insertionPoint;
            T tmp = curElem;
            memmove((void*)(insertionPoint + 1), (void*)insertionPoint, sizeof(T) * toMove);
            *insertionPoint = tmp;
        }
        ++sorted;
    }
}