#ifndef MERGESORT_H
#define MERGESORT_H

#include "helpers.h"
#include "insertionsort.h"

#include <iterator>
#include <vector>
#include <list>

#include <cstring>//for memcpy

//General version, works for any random access iterators, data to be sorted must have
//an assignment operator, no raw memcpy used, only assignment provided by sorted type
//somewhat slower that specialized for vectors and memcpy version
template <typename Iter> void mergesort(const Iter& begin, const Iter& end);

//Data type must be memcpyable, Container type must be a vector
template <typename Vec> void mergesort_vector_memcpy(Vec& a);

//Works for any container. Intermediate vector is constructed and than sorted.
//Data type must be memcpyable.
template <typename Container> void mergesort_through_vec(Container& l);

#include "mergesort.hpp"

#endif // MERGESORT_H
