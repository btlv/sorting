#pragma once

#include "helpers.h"

#include <vector>

template<typename Iter> inline void shiftDown(const Iter& begin, const Iter& toShift, const Iter& end)
{
    size_t pos = toShift - begin;
    size_t size = end - begin;
    Iter root = toShift;
    size_t firstChild = 2 * pos + 1;
    while (firstChild < size)
    {
        Iter toSwap(root);
        Iter fch = begin + firstChild;
        if(*toSwap < *fch)
            toSwap = fch;
        Iter secondChild = fch + 1;
        if(secondChild < end && *toSwap < *secondChild)
            toSwap = secondChild;

        if(toSwap != root)
            mswap(*root, *toSwap);
        else
            return;

        root = toSwap;
        pos = root - begin;
        firstChild = 2 * pos + 1;
    }
}

template <typename Iter> inline void heapify(const Iter& b, const Iter& e)
{
    size_t size = e - b;
    if(size < 2)
        return;
    Iter start = b + ((size - 2) / 2);
    while (true)
    {
        shiftDown(b, start, e);
        if(start == b)
            return;
        --start;
    }
}

template <typename Iter> void heapsort(const Iter& b, const Iter& e)
{
    size_t size = e - b;
    if(size < 2)
        return;
    heapify(b, e);
    Iter l(e - 1);
    while (b != l)
    {
        mswap(*l, *b);
        shiftDown(b, b, l);
        --l;
    }
}
